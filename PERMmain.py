############################################### IMPORT MODULES ###############################################

import numpy as np
np.seterr(divide='ignore', invalid='ignore')
import matplotlib.pyplot as plt
#from anim import make_3d_animation
import scipy as sci
from scipy import integrate
from scipy import optimize
from scipy.optimize import curve_fit
import math

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.colors import cnames
from matplotlib import animation
from IPython.display import HTML

import importlib
import zerofunctions3 as z
importlib.reload(z)
import finalfunctions2 as f
importlib.reload(f)

################################################# PARAMETERS & INITIALIZATION #############################################
N=1                            # number of polymers
L=250                          # number of beads per polymer
dim=2                          # dimensions (2D)
d=1                            # distance between the beads
k_b=1                          # boltzmann constant
T=25                           # temperature 
eps=0.25                       # value for epsilon in lennard jones
sigma=0.8                     # value for sigma in lennard jones
Ntheta= 6                      # number of divisions of theta
alpha1= 1.2                    # lower limit
alpha2= 2.2                      # upper limit


################################## BUILDING THE POLYMER #####################################

def perm(L):
    """
    function: Builds a single polymer using the Rosenbluth algorithm
    input: L
    output: Single polymer configuration with L beads
    """
    
    theta, pos_position, position, distance, radius, E_theta, w_l, sumW_l, w_lfinal, n, probability, e2e, w_ave, lowlimit, uplimit, W = z.zeromatrices(L, dim, Ntheta)
    position[0], position[1], theta = z.initialized(Ntheta, position, theta)
    PolWeight = 1
    NewWeight = 1 
    
    
    for i in range(2,L):
        pos_position = f.roulette(i, d, Ntheta, position, theta)
        E_theta = f.lennardjones(eps, sigma, Ntheta, pos_position, position[:i-1,:])
        w_lfinal[i], sumW_l[i],  position[i,:], n = f.extrabead(i, k_b, T, Ntheta, E_theta, sumW_l, w_lfinal, position, pos_position) 
        
        PolWeight = NewWeight * w_lfinal[i]
        W[i] = PolWeight
        lowlimit, uplimit=f.upperlowerlimit(i, k_b, T, alpha1, alpha2, w_lfinal, w_ave, lowlimit, uplimit)
        
        if (PolWeight > uplimit) and i>4:
            NewWeight, newchain = f.enrich(PolWeight, NewWeight, position[:i])
        
        elif (PolWeight < lowlimit):
            stopcondition = False
            NewWeight, stopcondition = f.prune(PolWeight, NewWeight, stopcondition)
            
        else:
            NewWeight=PolWeight

        e2e[i] = f.endtoend(position, i)

    return position, e2e, W

 

