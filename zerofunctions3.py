############################################### IMPORT MODULES ###############################################

import numpy as np
np.seterr(divide='ignore', invalid='ignore')
import matplotlib.pyplot as plt
#from anim import make_3d_animation
import scipy as sci
from scipy import integrate
from scipy import optimize
from scipy.optimize import curve_fit
import math

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.colors import cnames
from matplotlib import animation
from IPython.display import HTML

def zeromatrices(L, dim, Ntheta):
    """
    function: creates the zero matrices
    input: none
    output: zero matrices for all variables
    """
    dim=2
    theta= np.zeros(shape=(L-1), dtype=float)                               # L-1 angles per polymer, N polymers
    pos_position=np.zeros(shape=(Ntheta, dim), dtype=float)
    position=np.zeros(shape=(L, dim), dtype=float)             # x,y values for the 2D beads
    distance=np.zeros(shape=(L, dim), dtype=float) #Deze moet size (LxLxdim) worden want ... zie radius
    radius=np.zeros(shape=(L,Ntheta), dtype=float) #Deze moet size (LxL) worden want elke bead heeft een radius met elke bead!
    E_theta=np.zeros(shape=(L,Ntheta), dtype=float) #Moet naar size (6 x L) want E_theta bij elke theta, voor elke bead
    w_l=np.zeros(shape=(L,Ntheta), dtype=float) #Moet naar (6 x L)
    sumW_l=np.zeros(shape=(L), dtype=float) #Moet naar (L) want sommatie over 6
    w_lfinal = np.zeros(shape=(L,1), dtype=float)
    n = np.zeros(shape=(Ntheta-1), dtype=int)
    probability=np.zeros(shape=(Ntheta-1), dtype=float) 
    e2e = np.zeros(shape=(L))
    w_ave=np.zeros(shape=(L+1), dtype=float)
    lowlimit=np.zeros(shape=(L+1), dtype=float)
    uplimit=np.zeros(shape=(L+1), dtype=float)
    W = np.zeros(shape=(L))
    
    return theta, pos_position, position, distance, radius, E_theta, w_l, sumW_l, w_lfinal, n,  probability, e2e, w_ave, lowlimit, uplimit, W

def zeromatrices_e2e(min, max, repeat):
    
    ldata = np.zeros(shape=(max-min+1, repeat))
    e2edata = np.zeros(shape=(max-min+1, repeat))
    weightdata = np.zeros(shape=(max-min+1, repeat))
    meane2e = np.zeros(shape=(max-min+1))
    angledata = np.zeros(shape=(max-min+1, repeat))
    
    return ldata, e2edata, weightdata, meane2e
    
def initialized(Ntheta, position, theta):
    """
    function: initializes the initial positions
    input: zero matrices
    output: initial position and possible theta's 
    """
   
    position[0]=[0,0]
    position[1]=[1,0]
    theta=np.linspace(0, 2*np.pi - 2*np.pi/Ntheta, Ntheta).T
    
    return position[0], position[1], theta